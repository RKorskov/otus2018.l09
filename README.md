-*- mode: markdown; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
-*- eval: (set-language-environment Russian) -*-  
--- Time-stamp: <2019-07-29 13:15:06 korskov>

Overview
========

Группа 2018-12  
Студент:  
[Roman Korskov (Роман Корсков)](https://gitlab.com/RKorskov/otus-java_2018-12)  
korskov.r.v@gmail.com  

[Разработчик Java  
Курс об особенностях языка и платформы Java, стандартной библиотеке, о
проектировании и тестировании, о том, как работать с базами, файлами,
веб-фронтендом и другими
приложениями](https://otus.ru/lessons/razrabotchik-java/)

Домашние Работы
===============

# L9 JSON object writer #

Напишите свой json object writer (object to JSON string) аналогичный gson на основе javax.json или simple-json и Reflection.  
Поддержите:
1. массивы объектов;
2. массивы примитивных типов;
3. и коллекции из стандартный библиотерки.


ToDo
====

1. Наименование пакетов. Хотя формально правильно, но в мире Java принят другой подход. Нашел небольшую статейку http://src-code.net/pravila-imenovaniya-paketov/
2. Разделение функциональности по классам. То что у вас класс JSONator отвечает так же и за запуск приложения - не есть хорошо. поинтересуйтесь концепцией SOLID, которая имеет сильное хождение в мире ООП. Вынесите запуск приложения в отдельный класс
3. Наименование переменных. Как уже обсуждалось с вами раньше - вы слишком экономите на этом. В мире Java не принято экономить на этом, этим займется компилятор при формировании байт-кода. 
4. названия методов. В Java принято названия переменных и методов начинать со строчной буквы.
5. Логгирование в Java вы уже проходили. System.out.println и ex.printStackTrace() - это поводы чтобы ваш PullRequest был отказан без подробного изучения.
6. Тесты. Сама ваша работа с тестами интересна. Достойно. Я даже готов закрыть глаза на формат наименования методов. Единственное, не готов закрыть глаза на название вспомогательных классов, они вообще ни о чем не говорят.


1. uuidgen吗;
2. выпилено;
3. S.E.P.;
4. oops...;
5. выпилено;
6. перепилено в TestSuite_JSONator;

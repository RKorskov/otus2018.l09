// -*- coding: utf-8; indent-tabs-mode: nil; word-wrap: t; mode: java; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-07-29 13:13:27 korskov>

package Otus2018.L09;

// JUnit 4.x
import org.junit.*;
import static org.junit.Assert.*;

import java.lang.IllegalAccessException;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;

import Otus2018.L09.JSONator;

public class TestSuite_JSONator {
    /**
     * handling numbers (and primitives)
     */
    @Test
    public void test_number() throws IllegalAccessException {
        int i = 1;
        String jstr = JSONator.toJSON(i);
        //System.out.printf("primitive 《%s》\n", jstr);
        assertTrue(jstr.equals("{ 1 }"));
    }

    /**
     * handling strings
     */
    @Test
    public void test_String() throws IllegalAccessException {
        final String str = "a string";
        final String jstr = JSONator.toJSON(str);
        //System.out.printf("String 《%s》\n", jstr);
        assertTrue(jstr.equals("{ \"" + str + "\" }"));
    }

    /**
     * handling arrays
     */
    @Test
    public void test_Array() throws IllegalAccessException {
        final int[] arr = {0,1,2,3};
        final String jstr = JSONator.toJSON(arr);
        //System.out.printf("array 《%s》\n", jstr);
        assertTrue(jstr.equals("{ [0, 1, 2, 3] }"));
    }

    /**
     * handling (class) objects
     */
    @Test
    public void test_Object() throws IllegalAccessException {
        final String jstr = JSONator.toJSON(new ObjectPrimitivesMixClass());
        //System.out.printf("object (class) 《%s》\n", jstr);
        assertTrue(jstr.equals("{ \"i\" : 1, \"l\" : 2, \"s\" : \"a str.\", \"a\" : [0, 1, 2] }"));
    }

    /**
     * handling (generic) collection - sets
     */
    @Test
    public void test_Collection() throws IllegalAccessException {
        final String jstr = JSONator.toJSON(new ObjectsSetMix());
        //System.out.printf("set 《%s》\n", jstr);
        assertTrue(jstr.equals("{ \"i\" : [9, 8, 7, -1], \"p\" : 3, \"q\" : 4, \"f\" : 0.7, \"sstr\" : {\"str 乙\", \"str 丙\", \"str 甲\"} }"));
    }

    /**
     * handling (generic) collection - maps
     */
    @Test
    public void test_Map() throws IllegalAccessException {
        final String jstr = JSONator.toJSON(new ObjectMapMix());
        //System.out.printf("map 《%s》\n", jstr);
        assertTrue(jstr.equals("{ \"b\" : true, \"d\" : 4.2, \"o\" : null, \"mis\" : {\"1\" : \"串号1\", \"2\" : \"串号2\", \"4\" : \"串号3\", \"8\" : \"串号4\"} }"));
    }

}

class ObjectPrimitivesMixClass {
    int i = 1;
    long l = 2;
    String s = "a str.";
    short[] a = {0,1,2};
}

class ObjectsSetMix {
    int[] i = {9,8,7,-1};
    short p = 3;
    byte q = 4;
    float f = 0.7f;
    Set <String> sstr;
    ObjectsSetMix() {
        sstr = new HashSet<String>();
        sstr.add("str 甲");
        sstr.add("str 乙");
        sstr.add("str 丙");
    }
}

class ObjectMapMix {
    boolean b = true;
    double d = 4.2;
    Object o = null;
    Map<Integer,String> mis;
    ObjectMapMix() {
        mis = new HashMap<Integer,String>();
        mis.put(1,"串号1");
        mis.put(2,"串号2");
        mis.put(4,"串号3");
        mis.put(8,"串号4");
    }
}

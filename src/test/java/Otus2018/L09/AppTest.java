// -*- coding: utf-8; indent-tabs-mode: nil; word-wrap: t; mode: java; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-07-16 12:45:24 korskov>

package Otus2018.L09;

// JUnit 4.x
// https://howtodoinjava.com/junit-4/
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
 
@Suite.SuiteClasses({
   TestSuite_JSONator.class,
})

public class AppTest {}

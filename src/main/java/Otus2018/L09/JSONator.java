// -*- coding: utf-8; indent-tabs-mode: nil; word-wrap: t; mode: java; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-07-29 11:33:19 korskov>

package Otus2018.L09;

/**
 * <h3>L9 JSON object writer</h3>

 * <p>
 * Напишите свой json object writer (object to JSON string) аналогичный gson на основе javax.json или simple-json и Reflection.<br>
 * Поддержите:
 * <ol>
 * <li> массивы объектов;
 * <li> массивы примитивных типов;
 * <li> коллекции из стандартный библиотерки (List, Set, Map).
 * </ol>

 * https://json.org/
 */

import java.lang.Class;
import java.lang.StringBuilder;
import java.lang.reflect.Field;
import java.lang.reflect.Array;
import java.lang.IllegalAccessException;
import java.util.Collection;
import java.util.Map;

public class JSONator {

    public static String object2JStr(final Object obj) throws IllegalAccessException {
        StringBuilder jstr = new StringBuilder();
        if (obj == null)
            jstr.append("null");
        else if (obj instanceof Collection)
            jstr.append(collection2JStr(obj));
        else if (obj instanceof Map)
            jstr.append(map2JStr(obj));
        else {
            Class<?> oclz = obj.getClass();
            if(oclz.isArray())
                jstr.append(array2JStr(obj));
            else
                switch(oclz.getSimpleName()) {
                case "boolean":
                case "byte":
                case "short":
                case "int":
                case "long":
                case "float":
                case "double":
                case "Boolean":
                case "Byte":
                case "Short":
                case "Integer":
                case "Long":
                case "Float":
                case "Double":
                    jstr.append(number2JStr(obj));
                    break;
                case "char":
                case "Char":
                case "String":
                    jstr.append(string2JStr(obj));
                    break;
                default:
                    jstr.append(fields2JStr(obj));
                }
        }
        return jstr.toString();
    }


    private static String fields2JStr(final Object obj) throws IllegalAccessException {
        StringBuilder jstr = new StringBuilder();
        Field[] oflds = obj.getClass().getDeclaredFields();
        String dlm = null;
        for(Field ofld : oflds) {
            if(dlm != null)
                jstr.append(dlm);
            else
                dlm = ", ";
            jstr.append(enquoteString(ofld.getName()));
            jstr.append(" : ");
            Object fobj = ofld.get(obj);
            if (fobj == null)
                jstr.append("null");
            else if (fobj instanceof Collection)
                jstr.append(collection2JStr(fobj));
            else if (fobj instanceof Map)
                jstr.append(map2JStr(fobj));
            else if (fobj.getClass().isArray())
                jstr.append(array2JStr(fobj));
            else
                switch (fobj.getClass().getSimpleName()) {
                case "boolean":
                case "byte":
                case "short":
                case "int":
                case "long":
                case "float":
                case "double":
                case "Boolean":
                case "Byte":
                case "Short":
                case "Integer":
                case "Long":
                case "Float":
                case "Double":
                    jstr.append(number2JStr(fobj));
                    break;
                case "char":
                case "Char":
                case "String":
                    jstr.append(string2JStr(fobj));
                    break;
                default:
                    jstr.append(object2JStr(fobj));
                }
        }
        return jstr.toString();
    }

    public static String collection2JStr(final Object obj) throws IllegalAccessException {
        // incompatible types: java.lang.Class<capture#1 of ?> cannot be converted to java.util.Collection
        Collection <Object> ocl = (Collection <Object>) obj;
        StringBuilder jstr = new StringBuilder("{");
        String dlm = null;
        for(Object cobj : ocl) {
            if(dlm != null)
                jstr.append(dlm);
            else
                dlm = ", ";
            jstr.append(object2JStr(cobj));
        }
        jstr.append('}');
        return jstr.toString();
    }

    public static String map2JStr(final Object obj) throws IllegalAccessException {
        Map<?,?> omap = (Map<?,?>) obj;
        StringBuilder jstr = new StringBuilder("{");
        String dlm = null;
        for(Object kobj : omap.keySet()) {
            if(dlm != null)
                jstr.append(dlm);
            else
                dlm = ", ";
            //System.out.printf("--> '%s' : '%s'\n", kobj, omap.get(kobj));
            jstr.append(enquoteString(object2JStr(kobj)));
            jstr.append(" : ");
            jstr.append(object2JStr(omap.get(kobj)));
        }
        jstr.append('}');
        return jstr.toString();
    }

    public static String array2JStr(final Object obj) throws IllegalAccessException {
        StringBuilder jstr = new StringBuilder("[");
        int n = Array.getLength(obj) - 1;
        for(int i = 0; i < n; ++i) {
            jstr.append(object2JStr(Array.get(obj, i)));
            jstr.append(", ");
        }
        jstr.append(object2JStr(Array.get(obj, n)));
        jstr.append(']');
        return jstr.toString();
    }

    /*
     * Эскапизм эскапистов:
     * \" \\ \/ \b \f \n \r \t \u0000
     */
    public static String string2JStr(final Object obj) throws IllegalAccessException {
        // todo: escaping!
        StringBuilder jstr = new StringBuilder("\"");
        jstr.append(obj);
        jstr.append('"');
        return jstr.toString();
    }

    public static String number2JStr(final Object obj) throws IllegalAccessException {
        StringBuilder jstr = new StringBuilder();
        jstr.append(obj);
        return jstr.toString();
    }

    public static String enquoteString(final String str) {
        StringBuilder qstr = new StringBuilder("\"");
        for(char c : str.toCharArray()) {
            /*
            // 这是todo吗
            if (c.isUnicode()) {
                qstr.append("\\u");
                ...
            }
            else */
            /*
            if (c < 0x20) {
                qstr.append('\\');
                qstr.append(c | 0x40);
            }
            else */
            switch(c) {
            case '"':
                qstr.append("\\\"");
                break;
            case '\\':
                qstr.append("\\\\");
                break;
            case '/':
                qstr.append("\\/");
                break;
            case '\b':
                qstr.append("\\\b");
                break;
            case '\f':
                qstr.append("\\\f");
                break;
            case '\n':
                qstr.append("\\\n");
                break;
            case '\r':
                qstr.append("\\\r");
                break;
            case '\t':
                qstr.append("\\\t");
                break;
            default:
                qstr.append(c);
            }
        }
        qstr.append('"');
        return qstr.toString();
    }

}
